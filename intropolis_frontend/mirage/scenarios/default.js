export default function (server) {
  server.createList('sample', 5, {
    sraProjectAccessionNumber: 'SRP031496',
    sraSampleAccessionNumber: 'SRS491824',
    sraExperimentAccessionNumber: 'SRX365450',
    sraRunAccessionNumber: 'SRR1013437'
  });
  server.createList('sample', 5, {
    sraProjectAccessionNumber: 'SRP022133',
    sraSampleAccessionNumber: 'SRS418750',
    sraExperimentAccessionNumber: 'SRX275444',
    sraRunAccessionNumber: 'SRR846947'
  })

  server.createList('junction', 10)
}
