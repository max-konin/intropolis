import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  chromosome: 'chr_1',
  intronStart() { return faker.random.number() },
  intronEnd() { return faker.random.number() },
  strand() { return faker.random.arrayElement(['+', '-']) },
  donorDinucleotide() { return faker.random.arrayElement(['GT', 'AG']) },
  acceptorDinucleotide() { return faker.random.arrayElement(['GT', 'AG']) },

  sampleConnectors() {
    return [
      {count: faker.random.number(), sample_id: 1},
      {count: faker.random.number(), sample_id: 2}
    ]
  }
});
