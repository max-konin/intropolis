export default function() {

  this.get('/samples', function (db) {
    let records = db.samples.all();
    let json = this.serialize(records);
    json.meta = {page: 1, total_pages: 10};
    return json;
  });

  this.get('/junctions', function (db) {
    let records = db.junctions.all();
    let json = this.serialize(records);
    json.meta = {page: 1, total_pages: 10};
    return json;
  });
  this.get('samples/:id');
  this.get('junctions/:id');
}
