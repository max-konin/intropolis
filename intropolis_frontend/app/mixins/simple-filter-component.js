import Mixin from '@ember/object/mixin';
import { on } from '@ember/object/evented';
import { observer } from '@ember/object';
import { isBlank } from '@ember/utils';

export default Mixin.create({
  _onInit: on('didReceiveAttrs', function () {
    this.set('valueWrapper', this.get('value'));
    this.get('onValueChange')(this.get('key'), this.get('valueWrapper'));
  }),

  _valueChanged: observer('valueWrapper', function () {
    if (isBlank(this.get('valueWrapper'))) {
      this.set('valueWrapper', null);
    }
    this.get('onValueChange')(this.get('key'), this.get('valueWrapper'));
  })
});
