import Mixin from '@ember/object/mixin';
import { isPresent } from '@ember/utils';

export default Mixin.create({
  // override in route to transform filter params, see app/routes/general/translators/index.js for an example
  _modifyFilterParams(params, query) {
    return query;
  },

  _filterQuery(params) {
    const query = {};
    if (isPresent(this.get('baseFilterParams'))) {
      this.get('baseFilterParams').forEach(function (key) {
        const value = params[key];
        if (isPresent(value)) {
          query[key.underscore()] = value;
        }
      });
    }
    return this._modifyFilterParams(params, query);
  },

  model(params) {
    const fullQuery = {
      filter:         this._filterQuery(params),
      orderColumn:    params['orderColumn'],
      orderDirection: params['orderDirection']
    };
    return this.findPaged(this.get('modelName'), fullQuery);
  }
});
