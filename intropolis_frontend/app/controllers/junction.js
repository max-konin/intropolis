import Controller from '@ember/controller';
import { map, mapBy } from '@ember/object/computed';
import { get, computed } from '@ember/object';

export default Controller.extend({
  samples: map('model.sampleConnectors', function (sample) {
    return {
      count: sample.count,
      data: this.store.findRecord('sample', sample.sample_id)
    }
  }),

  sampleModels: mapBy('samples', 'data'),

  canRenderChart: computed('sampleModels.@each.isFulfilled', function () {
    return this.sampleModels.every((el) => el.isFulfilled);
  }),

  aggregatedResults: map('samples', function (sample) {
    return { y: parseInt(sample.count), name: get(sample, 'data.sraRunAccessionNumber') };
  }),

  samplesChartData: computed('aggregatedResults', function () {
    return [{ data: this.get('aggregatedResults'), name: 'Total' }];
  }),

  samplesChartOptions: Object.freeze({
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Samples'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
        }
      }
    },
  }),

});
