import Controller from '@ember/controller';

export default Controller.extend({
  queryParams: ['page', 'perPage', 'sraProjectAccessionNumber', 'sraSampleAccessionNumber',
                'sraExperimentAccessionNumber', 'sraRunAccessionNumber'],
  sraProjectAccessionNumber: null,
  sraSampleAccessionNumber: null,
  sraExperimentAccessionNumber: null,
  sraRunAccessionNumber: null,

  actions: {
    search(query) {
      query['page'] = 1,
      this.transitionToRoute({ queryParams: query });
    },
  }
});
