import Controller from '@ember/controller';

export default Controller.extend({
  queryParams: ['page', 'perPage', 'chromosome', 'intronStart', 'intronEnd'],
  chromosome: null,
  intronStart: null,
  intronEnd: null,

  actions: {
    search(query) {
      query['page'] = 1,
      this.transitionToRoute({ queryParams: query });
    },
  }
});
