import Component from '@ember/component';
import { isPresent } from '@ember/utils';

export default Component.extend({
  classNames: ['filters-block'],

  init() {
    this._super(...arguments);
    this.set('activeFilters', {});
  },

  actions: {
    pushValue(key, value) {
      if (isPresent(value)) {
        this.get('activeFilters')[key] = value;
      } else {
        this.get('activeFilters')[key] = null;
      }
    },

    search() {
      this.get('search')(this.get('activeFilters'));
    },

    reset() {
      for (let key in this.get('activeFilters')) {
        this.get('activeFilters')[key] = null;
      }
      this.get('search')(this.get('activeFilters'));
    }
  }
});
