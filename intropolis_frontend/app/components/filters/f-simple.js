import Component from '@ember/component';
import SimpleFilterComponentMixin from 'intropolis-frontend/mixins/simple-filter-component';

export default Component.extend(SimpleFilterComponentMixin, {
  classNames: ['filter', 'f-simple']
});
