import Component from '@ember/component';
import PagedRemoteArray from 'ember-cli-pagination/remote/paged-remote-array';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
  page: 1,
  minDetectCount: null,
  intronEnd: null,
  intronStart: null,

  store: service(),

  junctions: computed('page', 'minDetectCount', 'intronStart', 'intronEnd', function () {
    const params = {
      filter: {
        intron_start: this.intronStart,
        intron_end: this.intronEnd,
        sample_id: this.sample.id,
        count: this.minDetectCount
      }
    }
    return PagedRemoteArray.create({
      modelName: 'junction',
      store: this.store,
      page: this.page || 1,
      perPage: 10,
      otherParams: params
    });
  })
});
