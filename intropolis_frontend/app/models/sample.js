import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  sraProjectAccessionNumber: DS.attr('string'),
  sraSampleAccessionNumber: DS.attr('string'),
  sraExperimentAccessionNumber: DS.attr('string'),
  sraRunAccessionNumber: DS.attr('string'),

  name: computed('sraProjectAccessionNumber', 'sraSampleAccessionNumber',
  'sraExperimentAccessionNumber', 'sraRunAccessionNumber', function () {
    return `${this.sraProjectAccessionNumber}/${this.sraSampleAccessionNumber}/` +
           `${this.sraExperimentAccessionNumber}/${this.sraRunAccessionNumber}`
  })
});
