import DS from 'ember-data';

export default DS.Model.extend({
  chromosome: DS.attr('string'),
  intronStart: DS.attr('string'),
  intronEnd: DS.attr('string'),
  strand: DS.attr('string'),
  donorDinucleotide: DS.attr('string'),
  acceptorDinucleotide: DS.attr('string'),

  sampleConnectors: DS.attr()
});
