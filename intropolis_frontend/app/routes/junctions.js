import Route from '@ember/routing/route';
import RouteMixin from 'ember-cli-pagination/remote/route-mixin';
import IndexRouteMixin from 'intropolis-frontend/mixins/index-route';

export default Route.extend(RouteMixin, IndexRouteMixin, {
  queryParams: {
    chromosome:    { refreshModel: true },
    intronStart:     { refreshModel: true },
    intronEnd: { refreshModel: true }
  },

  baseFilterParams: Object.freeze([
    'chromosome', 'intronStart', 'intronEnd'
  ]),

  modelName: 'junction'
});
