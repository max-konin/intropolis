import Route from '@ember/routing/route';
import RouteMixin from 'ember-cli-pagination/remote/route-mixin';
import IndexRouteMixin from 'intropolis-frontend/mixins/index-route';

export default Route.extend(RouteMixin, IndexRouteMixin, {
  queryParams: {
    sraProjectAccessionNumber:    { refreshModel: true },
    sraSampleAccessionNumber:     { refreshModel: true },
    sraExperimentAccessionNumber: { refreshModel: true },
    sraRunAccessionNumber:        { refreshModel: true },
    roamingProvider:              { refreshModel: true },
  },

  baseFilterParams: Object.freeze([
    'sraProjectAccessionNumber', 'sraSampleAccessionNumber',
    'sraExperimentAccessionNumber', 'sraRunAccessionNumber'
  ]),

  modelName: 'sample'
});
