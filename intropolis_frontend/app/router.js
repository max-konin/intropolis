import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('junctions');
  this.route('junction', { path: '/junctions/:junction_id' });
  this.route('samples');
  this.route('sample', { path: '/samples/:sample_id' });
});

export default Router;
