FactoryBot.define do
  factory :junction do
    chromosome 'chr1'
    intron_start '1'
    intron_end '10'
    strand '+'
    donor_dinucleotide 'AT'
    acceptor_dinucleotide 'GT'
  end
end
