FactoryBot.define do
  factory :sample do
    old_id 1
    sra_project_accession_number 'SKA001'
    sra_sample_accession_number 'MRAZ32'
    sra_experiment_accession_number 'EBA101'
    sra_run_accession_number 'LOH666'
  end
end
