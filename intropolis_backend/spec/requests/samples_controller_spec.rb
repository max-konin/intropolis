require 'rails_helper'

RSpec.describe SamplesController, type: :request do
  describe "GET /samples" do
    let!(:sample) { create :sample }
    let(:parsed_response) { JSON.parse response.body }

    subject { get samples_path, params: { page: 1, per_page: 1 } }

    before { subject }

    it { expect(response).to have_http_status(200) }
    it { expect(parsed_response['meta']['page']).to eq '1' }
    it { expect(parsed_response['meta']['total_pages']).to eq 1 }

    context 'filter by chromosome' do
      let!(:sample) { create :sample, sra_run_accession_number: 'LOL303' }
      let!(:other_sample) { create :sample, sra_run_accession_number: 'VASYA1' }

      subject { get samples_path, params: { filter: { sra_run_accession_number: 'LOL303' } } }

      before { subject }

      it { expect(parsed_response['data'].count).to eq(1) }
    end
  end
end
