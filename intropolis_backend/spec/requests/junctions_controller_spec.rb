require 'rails_helper'

RSpec.describe JunctionsController, type: :request do
  describe 'GET /junctions' do
    let(:connectors) { build_list :sample_connector, 3 }
    let!(:junction) { create :junction, sample_connectors: connectors }
    let(:parsed_response) { JSON.parse response.body }

    subject { get junctions_path, params: { page: 1, per_page: 1 } }

    before { subject }

    it { expect(response).to have_http_status(200) }
    it { expect(parsed_response['meta']['page']).to eq '1' }
    it { expect(parsed_response['meta']['total_pages']).to eq 1 }

    context 'filter by chromosome' do
      let!(:junction) { create :junction, chromosome: 'a' }
      let!(:other_junction) { create :junction, chromosome: 'b' }

      subject { get junctions_path, params: { filter: { chromosome: 'a' } } }

      before { subject }

      it { expect(parsed_response['data'].count).to eq(1) }
    end

    context 'complex filters' do
      let!(:sample) { create :sample }
      let(:connector) { build :sample_connector, sample_id: sample.id}
      let!(:junction) { create :junction, sample_connectors: [connector] }

      context 'filter by sample_id' do
        let(:other_connector) { build :sample_connector }
        let!(:other_junction) { create :junction, sample_connectors: [other_connector] }

        subject { get junctions_path, params: { filter: { sample_id: sample.id } } }

        before { subject }

        it { expect(Junction.all.count).to eq(2) }
        it { expect(parsed_response['data'].count).to eq(1) }
      end

      context 'filter by count' do
        let(:other_connector) { build :sample_connector, count: 1 }
        let!(:other_junction) { create :junction, sample_connectors: [other_connector] }

        subject { get junctions_path, params: { filter: { sample_id: sample.id, count: 2 } } }

        before { subject }

        it { expect(Junction.all.count).to eq(2) }
        it { expect(parsed_response['data'].count).to eq(1) }
      end
    end
  end
end
