class JunctionsController < ActionController::Base
  SIMPLE_FILTERS = %i[chromosome intron_start intron_end].freeze

  def index
    items = paginate(filter(Junction.all))
    render jsonapi: items, meta: meta(items)
  end

  def show
    render jsonapi: Junction.find(params[:id])
  end

  private

  def filter(query)
    query = SIMPLE_FILTERS.inject(query) { |acc, el| apply_simple_filters acc, el }
    query = filter_by_sample_connector(query)
    query
  end

  def apply_simple_filters(query, field)
    return query if params[:filter].blank?
    value = params[:filter][field]
    return query if value.blank?
    query.where(field => value)
  end

  def filter_by_sample_connector(query)
    return query if params[:filter].blank? || params[:filter][:sample_id].blank?
    count = params[:filter][:count].blank? ? 0 : params[:filter][:count]
    query.where(sample_connectors: {
                 '$elemMatch':
                   {
                     sample_id: BSON::ObjectId(params[:filter][:sample_id]),
                     count: { '$gte': count.to_i }
                   }
                })
  end

  def meta(items)
    {
      page: params[:page],
      total_pages: items.try(:total_pages)
    }
  end

  def paginate(query)
    return query unless need_paginate?

    query.page(params[:page]).per(params[:per_page] || 10)
  end

  def need_paginate?
    params[:page].present?
  end
end
