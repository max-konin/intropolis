class SamplesController < ApplicationController
  SIMPLE_FILTERS = %i[sra_project_accession_number sra_sample_accession_number
                      sra_experiment_accession_number sra_run_accession_number].freeze

  def index
    items = paginate(filter(Sample.all))
    render jsonapi: items, meta: meta(items)
  end

  def show
    render jsonapi: Sample.find(params[:id])
  end

  private

  def filter(query)
    query = SIMPLE_FILTERS.inject(query) { |acc, el| apply_simple_filters acc, el }
    query
  end

  def apply_simple_filters(query, field)
    return query if params[:filter].blank?
    value = params[:filter][field]
    return query if value.blank?
    query.where(field => value)
  end

  def meta(items)
    {
      page: params[:page],
      total_pages: items.try(:total_pages)
    }
  end

  def paginate(query)
    return query unless need_paginate?

    query.page(params[:page]).per(params[:per_page] || 10)
  end

  def need_paginate?
    params[:page].present?
  end
end
