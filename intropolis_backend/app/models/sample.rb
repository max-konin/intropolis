class Sample
  include Mongoid::Document

  field :old_id
  field :sra_project_accession_number
  field :sra_sample_accession_number
  field :sra_experiment_accession_number
  field :sra_run_accession_number

end
