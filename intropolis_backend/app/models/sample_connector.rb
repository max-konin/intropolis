class SampleConnector
  include Mongoid::Document

  field :sample_id
  field :count, type: Integer
end
