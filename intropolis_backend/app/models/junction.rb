class Junction
  include Mongoid::Document

  field :chromosome
  field :intron_start
  field :intron_end
  field :strand
  field :donor_dinucleotide
  field :acceptor_dinucleotide

  embeds_many :sample_connectors

end
