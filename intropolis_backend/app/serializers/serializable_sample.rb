class SerializableSample < JSONAPI::Serializable::Resource
  type 'samples'

  attributes :old_id, :sra_project_accession_number, :sra_sample_accession_number,
             :sra_experiment_accession_number, :sra_run_accession_number
end
