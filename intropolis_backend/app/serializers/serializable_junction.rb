class SerializableJunction < JSONAPI::Serializable::Resource
  type 'junctions'

  attributes :chromosome, :intron_start, :intron_end, :donor_dinucleotide, :acceptor_dinucleotide, :strand

  attribute :sample_connectors do
    @object.sample_connectors.as_json
  end
end
