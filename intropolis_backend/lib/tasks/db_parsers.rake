require 'tsv'
require 'ruby-progressbar'

namespace :parsers do
  desc 'Parses samples table'
  task parse_samples: :environment do
    tsv_file = TSV.parse_file(Rails.root.join('data', 'samples.tsv')).without_header

    tsv_file.each_slice(200) do |batch|
      batch.each do |row|
        Sample.create(old_id: row[0],
                      sra_project_accession_number: row[1],
                      sra_sample_accession_number: row[2],
                      sra_experiment_accession_number: row[3],
                      sra_run_accession_number: row[4])
      end
    end
  end

  desc 'Parses junctions table'
  task parse_junctions: :environment do
    tsv_file = TSV.parse_file(Rails.root.join('data', 'junctions.tsv')).without_header

    tsv_file.each do |row|
      junction = Junction.new(chromosome: row[0],
                              intron_start: row[1],
                              intron_end: row[2],
                              strand: row[3],
                              donor_dinucleotide: row[4],
                              acceptor_dinucleotide: row[5])

      old_sample_ids = row[6].split(',')
      samples = Sample.where(:old_id.in => old_sample_ids).to_a
      connectors = old_sample_ids.zip(row[7].split(',')).map do |connector|
        sample_id = samples.find { |s| s.old_id == connector[0] }.id
        SampleConnector.new(sample_id: sample_id,
                            count: connector[1])
      end
      junction.sample_connectors = connectors
      junction.save
    end
  end
end

