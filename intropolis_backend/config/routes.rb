Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :samples, only: %i[index show]
  resources :junctions, only: %i[index show]
end
