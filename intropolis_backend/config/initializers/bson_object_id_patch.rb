module BSON
  class ObjectId
    def as_json(*_args)
      to_s
    end
  end
end
